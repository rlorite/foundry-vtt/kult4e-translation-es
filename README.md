## Description - Descripción  

* Spanish translation for the Kult DL system by fattom23.
----
* Traducción del sistema para Kult DL de fattom23.

## Installation - Instalación  

* Install/Update _Kult DL_ system, by fattom23, from Foundry's system manager.
[System Url](https://foundryvtt.com/packages/Kult4e)
* Install/Update _Babele_ module from Foundry's module manager.
[Module Url](https://foundryvtt.com/packages/babele/)
* (Optional) Install/Update _Translation: Spanish [Core]_ module from Foundry's module manager:
[Module Url](https://foundryvtt.com/packages/FoundryVTT-ES/)
* Install/Update _Kult DL Spanish Translation_ module from Foundry's module manager:
[Module Url](https://gitlab.com/rlorite/foundry-vtt/kult4e-translation-es/-/raw/master/module.json)
* Inside the Game World, at _Configuration/Manage Modules_ activate Babele y both translations.
* At _Configuration/Setup_ change language to Spanish.
----
* Instale/Actualice el sistema _Kult DL_, de fattom23, desde el gestor de sistemas de Foundry.
[Module Url](https://foundryvtt.com/packages/Kult4e)
* Instale/Actualice el módulo _Babele_ desde el gestor de módulos de Foundry.
[Module Url](https://foundryvtt.com/packages/babele/)
* (Opcional) Instale/Actualice el módulo _Translation: Spanish [Core]_ desde el gestor de módulos de Foundry:
[Module Url](https://foundryvtt.com/packages/FoundryVTT-ES/)
* Instale/Actualice el módulo _Kult DL Spanish Translation_ desde el gestor de módulos de Foundry:
[Module Url](https://gitlab.com/rlorite/foundry-vtt/kult4e-translation-es/-/raw/master/module.json)
* Entre en el Mundo de Juego, _Configuración/Administrar Módulos_ active Babele y las dos traducciones.
* En _Configuración/Configurar Ajustes_ cambie el idioma a Spanish.

## Notificación de errores - Feedback  

* Please any suggestion or feedback is greatly appreciated. Use the issue system.
----
* Cualquier sugerencia o comentario es bienvenido. Usad el sistema de notificación de errores de github.

## Créditos  

* La traducción es obra de *Roberto Lorite* (Roberto Lorite#0408).
* *Babele* es un módulo de *Simone Ricciardi*.
