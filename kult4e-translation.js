Hooks.once('init', () => {
    if(typeof  Babele !== "undefined") {
        Babele.get().register({
            module: 'kult4e-translation-es',
            lang: 'es',
            dir: 'babele'
        });
    }
})
